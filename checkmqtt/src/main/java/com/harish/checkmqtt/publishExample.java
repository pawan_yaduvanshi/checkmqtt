package com.harish.checkmqtt;

import java.security.DomainCombiner;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.persist.PersistanceFileFilter;

public class publishExample implements IMqttActionListener {
	static MqttClient client;
	static MemoryPersistence persistence;

	public static void publishing() {
		byte[] arr = App.message.getBytes();
		persistence = new MemoryPersistence();

		try {
			client = new MqttClient(App.broker, App.clientId, persistence);
			MqttConnectOptions options = new MqttConnectOptions();
			options.setCleanSession(true);

			MqttMessage mqttMessage = new MqttMessage(arr);
			mqttMessage.setQos(App.qualityOfService);

			MqttTopic mqttTopic = client.getTopic(App.topic);
			System.out.println("Connecting to the client as a publisher");
			client.connect(options);
			System.out.println("Client connected and publishing message to the broker");
			mqttTopic.publish(mqttMessage);
			System.out.println(App.message + " published to the broker");
		} catch (MqttException e) {
			
			System.out.println("Error is Here!");
		}

	}

	public void onSuccess(IMqttToken asyncActionToken) {
		// TODO Auto-generated method stub
		System.out.println("Connections successfull as a publisher");

	}

	public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
		// TODO Auto-generated method stub
		System.out.println("Connection successfull as a publisher");
	}

}
