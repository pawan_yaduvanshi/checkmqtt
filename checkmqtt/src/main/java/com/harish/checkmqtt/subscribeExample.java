package com.harish.checkmqtt;

import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class subscribeExample implements MqttCallback,IMqttActionListener{
	MqttClient subscriberClient;
	MemoryPersistence persistence;
	
	
	public void name() {
		
	} 
	void subscriber() {
		try {
			persistence=new MemoryPersistence();
			subscriberClient=new MqttClient(App.broker,"temp",persistence);
			subscriberClient.setCallback(this);
						
			MqttConnectOptions options=new MqttConnectOptions();
			options.setCleanSession(true);
			
			System.out.println("Connecting to the client as a subscriber");
			subscriberClient.connect(options);
			System.out.println("Cliengt xconnected and subscribing");
			subscriberClient.subscribe(App.topic);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub
		
	}

	public void messageArrived(String topic, MqttMessage message) throws Exception {
		// TODO Auto-generated method stub
//		System.out.println("Arrived Message is : "+message);
		
		try {
		
//		byte[] arr=(byte[])message.getPayload();
		JsonNode node=App.mapper.readTree(message.getPayload());
//		String string=node.get("STN").asText();
//		System.out.println(message);
		BlockingQueue queue=new LinkedBlockingDeque();
		queue.add(node);
		System.out.println(queue);
		
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}
	public void onSuccess(IMqttToken asyncActionToken) {
		// TODO Auto-generated method stub
		System.out.println("Connection successfull");
		
	}
	public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
		// TODO Auto-generated method stub
		System.out.println("Not connected");
		
	}

}
